
#ifndef _VIRC_CRYPTO_H
#define _VIRC_CRYPTO_H


#include "mbedtls/base64.h"
#include "mbedtls/config.h"
#include "mbedtls/platform.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/bignum.h"
#include "mbedtls/x509.h"
#include "mbedtls/rsa.h"
#include "mbedtls/md.h"

#include "virc_core.h"

#define VIRC_CRYPTO_OK                  (0)
#define VIRC_CRYPTO_KEY_SIZE            (512)
#define VIRC_CRYPTO_KEY_EXPONENT        (65537)
#define VIRC_CRYPTO_PRIVKEY_FILENAME    ("/storage/rsa_priv.txt")
#define VIRC_CRYPTO_PUBKEY_FILENAME     ("/storage/rsa_pub.txt")
#define VIRC_CRYPTO_SIG_LENGTH          (64)
#define VIRC_CRYPTO_HASH_LENGTH         (128)


void virc_crypto_init(void);
bool virc_crypto_sign(uint8_t *buf, size_t len, uint8_t *sig);

#endif
